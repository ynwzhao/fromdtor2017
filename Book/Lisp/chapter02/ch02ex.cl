1.a.14 b.(1 5) c.7 d.(nil 3)
2.(cons 'a (cons 'b (cons 'c nil))),
	(cons 'a (list 'b 'c)) (cons 'a '(b c))
3.(defun our-forth (lst)
	(car (cdr (cdr (cdr lst)))))
4.(defun our-max (x y)
	(if (> x y)
		x
		y))
5.a.return nil
  b.x's first appear index of y,begin with 0
6.a.car
  b.or 
  c.apply此处使用funcall反而不能工作，应为它的参数不需要打包为列表
7.(defun our-sublist (lst)
	(if (null lst)
		nil
		(if (listp (car lst))
			lst
			(our-sublist (cdr lst)))))
8.
a;iteration
(defun print-dot (number)
	(do ((i 1 (+ i 1)))
		((> i number) 'done)
		(format t "~A" ".")))
;recursive
(defun print-dot (number)
	(if (eql number 0)
		'done
		(progn
			(format t "~A" ".")
			(print-dot (- number 1)))))

b;iteration
(defun our-times (lst)
	(let ((times 0))
		(dolist (obj lst)
			(if (eql 'a obj)
				(setf times (+ times 1))))
		times))
;recursive
(defun our-times (lst)
	(if (null lst)
		0
		(if (eql 'a (car lst))
			(+ (our-times (cdr lst)) 1)
			(our-times (cdr lst)))))
9.a
(defun summit (lst)
	(let ((x (remove nil lst)))
		(apply #'+ x)))
b.
(defun summit (lst)
	(if (null lst)
		0
		(let ((x (car lst)))
			(if (null x)
				(summit (cdr lst))
				(+ x (summit (cdr lst)))))))
