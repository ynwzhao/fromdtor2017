;~A placeholder ~% newline
(format t "~A plus ~A equals ~A. ~%" 2 3 (+ 2 3))

(defun askem (string)
	(format t "~A" string)
	(read))

;let local variable
(let ((x 1) (y 2))
	(+ x y))


(defun ask-number ()
	(format t "Please enter a number. ")
	(let ((val (read)))
		(if (numberp va)
			val
			(ask-number))))

;global variable
(defparameter *glob* 99)
;global constant
(defconstant limit (+ *glob* 1))

;check a symbol is a global or constant variable
>(boundp '*glob*)
T

;assignment
(setf *glob* 98)
(let ((n 10))
	(setf n 2)
	n)

;if (boundp x) is nil, x set to global variable
(setf x (list 'a 'b 'c))

(setf a b       (setf a b)
	  c d    =  (setf c d)
	  e f)      (setf e f)
	 
;iteration
(defun show-squares (start end)
	(do ((i start (+ i 1)))
		((> i end) 'done)
	 	(format t "~A ~A~%" i (* i i))))

;recursive
(defun show-squares (i end)
	(if (> i end)
		'done
		(progn
			(format t "~A ~A~%" i (* i i))
			(show-squares (+ i 1) end))))
			
(defun our-length (lst)
	(let ((len 0))
		(dolist (obj lst))
			(setf len (+ len 1))
		len))

(defun our-length (lst)
	(if (null lst)
		0
		(+ (our-length (cdr lst)) 1)))

"'" to quote 
as to
"#'" to function
;
>(apply #'+ '(1 2 3))
6
>(funcall #'+ 1 2 3)
6

(lambda (x y)
	(+ x y))
>((lambda (x) (+ x 100)) 1)
101

>(funcall #'(lambda (x) (+ x 100))
	1)
101
>(typep 27 'integer)
T



 




