(defparameter total nil)
(defparameter init-killer nil)
(defparameter gap+1 nil)
(defparameter prev nil)
(defparameter lst nil)

(defun kill-one (gap killer)
  (if (zerop gap)
      (if (eql (car lst) killer)
	  (progn
	    (setf lst (append (cdr lst) (list killer)))
	    (kill-one gap killer))
	  (setf prev (pop lst)))
      (progn
	(setf lst (append (cdr lst) (list (car lst))))
	(kill-one (1- gap) killer))))

(defun single? (lst)
  (and (car lst)
       (null (cdr lst))))

(defun kill (gap killer)
  (if (single? lst)
      (format t "Your place is ~A~%" prev)
      (progn
	(kill-one gap killer)
	(kill gap killer))))

(defun main () 
  (format t "Please enter the number of participants: ")
  (setf total (read))
  (format t "And the gap: ")
  (setf gap+1 (read))
  (format t "And the killer's initial place: ")
  (setf init-killer (read))

  (dotimes (i total)
    (push (1+ i) lst))
  (setf lst (nreverse lst))
  (kill (1- gap+1) init-killer))
