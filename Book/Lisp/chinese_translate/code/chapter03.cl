(defun our-listp (x)
	(or (null x)
		(consp x)))
;
(defun our-atom (x)
	(not (consp x)))
;nil is not only a list but also a atom
;eql:等同，同一
;equal:值相等
(defun our-equal (x y)
	(or (eql x y)
		(and (consp x)
			(consp y)
			(our-equal (car x) (car y))
			(our-equal (cdr x) (cdr y)))))

(setf x '(a b c)
	  y (copy-list x))

(defun our-cupy-list (lst)
	(if (atom lst)
	lst
	(cons (car lst) (our-cupy-list (cdr lst)))))

>(append '(a b) '(c d) 'e)
(A B C D E)
;;;;;;;;;;;;;;;;;;;;;
;compress
(defun compress (x)
	(if (consp x)
		(compr (car x) 1 (cdr x))
		x))
(defun compr (elt n lst)
	(if (null lst)
		(list (n-elts elt n))
		(let ((next (car lst)))
			(if (eql next elt)
				(compr elt (+ n 1) (cdr lst))
				(cons (n-elts elt n)
					(compr next 1 (cdr lst)))))))
(defun n-elts (elt n)
	(if (> n 1)
		(list n elt)
		elt))
;;;;;;;;;;;;;;;;;;;;;
;uncompress
(defun uncompress (lst)
	(if (null lst)
		nil
		(let ((elt (car lst)) (rest (uncompress (cdr lst))))
			(if (consp elt)
				(append (apply #'list-of elt) rest)
				(cons elt rest)))))
(defun list-of (n elt)
	(if (zerop n)
		nil
		(cons elt (list-of (- n 1) elt))))
;;;;;;;;;;;;;;;;;;;;;
(nth 0 '(a b c))
(nthcdr 2 '(a b c))

(defun our-nthcdr (n lst)
	(if (zerop n)
		lst
		(our-nthcdr (- n 1) (cdr lst))))

>(last '(a b c))
(C)
(first lst)=(nth 0 lst);(second lst)=(nth 1 lst)

>(mapcar #'(lambda (x) (+ x 10)) '(1 2 3))
(11 12 13)

>(mapcar #'list
	'(a b c)
	'(1 2 3 4))
((A 1) (B 2) (C 3))
>(maplist #'(lambda (x) x)
	'(a b c))
((A B C) (B C) (C))
;
(defun our-copy-tree (tr)
	(if (atom tr)
	tr
	(cons (our-copy-tree (car tr))
		(our-copy-tree (cdr tr)))))
		
(and (integerp x) (zerop (mod x 2)))

(substitute object lst)
;double recursive
(defun our-subst (new old tree)
	(if (eql tree old)
		new
		(if (atom tree)
			tree
			(cons (our-subst new old (car tree))
				  (our-subst new old (cdr tree))))))
(def len (lst)
	(if (null lst)
		0
		(+ (len (cdr lst)) 1)))

>(member 'b '(a b c))
(B C)
>(member '(a) '((a) (z)) :test #'equal)
((A) (Z))
>(member 'a '((a b) (c d)) :key #'car)
((A B) (C D))

(defun our-member-if (fn lst)
	(and (consp lst)
		(if (funcall fn (car lst))
			lst
			(our-member-if fn (cdr lst)))))

>(adjoin 'z '(a b c))
(Z A B C)
union intersection set-difference
length
subseq
reverse
(defun mirror? (s)
	(let ((len (length x)))
		(and (evenp len)
			(let ((mid (/ len 2)))
				(equal (subseq s 0 mid)
					(reverse (subseq s mid)))))))
;no side effects
(defun nthmost (n lst)
	(nth (- n 1)
		(sort (copy-list lst) #'>)))

every and some
(defun push (x lst)
	(setf lst (cons x lst)))
(defun pop (lst)
	(if (null lst)
		nil
		(let ((x (car lst)))
			(setf lst (cdr lst))
			x)))
			
(defun our-reverse (lst)
	(let ((acc nil))
		(dolist (elt lst)
			(push alt acc))
			acc))

(pushnew x lst)

(defun proper-list? (x)
	(or (null x)
		(and (consp x)
			(proper-list? (cdr x)))))
			
>(setf pair (cons 'a 'b))
(A . B)
>'(a . (b . (c . nil)))
(A B C)

>(setf trans '((+ . "add") (- . "substract")))
((+ . "add") (-. "substract"))

(defun our-assoc (key alist)
	(and (consp alist)
		(let ((pair (car alist)))
			(if (equal key (car pair))
				pair
				(our-assoc key (cdr alist))))))
				
(defun shortest-path (start end net)
	(bfs end (list (list start)) net))
(defun bfs (end queue net)
	(if (null queue)
	nill
	(let ((path (car queue)))
		(let ((node (car path)))
			(if (eql node end)
				(reverse path)
				(bfs end
					(append (car queue)
						(new-paths path node net))
					net))))))
(defun new-paths (path node net)
	(mapcar #'(lambda (n) (cons n path))
		(cdr (assoc node net))))


